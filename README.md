### Personal emacs conf using guix manifest

### How to use
```
git clone https://github.com/MiroYld/emacs-conf ~/conf/emacs  
manually--> guix shell -m ~/emacs/conf/emacs-manifest.scm -- emacs  
using alias --> alias emacs="guix shell -m ~/emacs/conf/emacs-manifest.scm -- emacs"  
```
